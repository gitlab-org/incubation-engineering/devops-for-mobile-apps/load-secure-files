# load-secure-files

# This project has been deprecated, please use https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files instead.

## Background

The load-secure-files project exists to create a simple way to integrate a GitLab Runner with the [Secure Files](https://gitlab.com/gitlab-org/gitlab/-/issues/346290) feature in GitLab. Please note: this feature is still under active development. Please report any bugs or problems by creating an issue in the [DevOps for Mobile Apps project](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/readme/-/issues).


## Getting started

The quickest way to get started is to add the following line at the start of your CI script in your `.gitlab-ci.yml` file. 

```
curl -s https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/load-secure-files/-/raw/main/installer | bash
```

This command will download and install a script that will download all of the Secure Files for the project into the `.secure_files` directory in the runner instance.

If you'd like to change the location where the files are downloaded, simply set the `SECURE_FILES_DOWNLOAD_PATH` environment variable. This can be done in either the `.gitlab-ci.yml` file, or as a CI variable.

## How it works

This project uses [Traveling Ruby](http://foobarwidget.github.io/traveling-ruby/) to build a self contained package that can be run on Linux, OS X, or Windows without having to install additional dependencies. 

The `installer` script will attempt to detect the architecture of the machine and download the appropriate package. Once downloaded it will automatically invoke the `load.rb` script to download the Secure Files.

If for some reason the `installer` script doesn't work on your architecture, downloading and executing the `load.rb` manually should work as well. 

## Sample config

Below is a simple `.gitlab-ci.yml` file that can be used to test the feature

```
stages:
  - test

test:
  script: 
    - curl -s https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/load-secure-files/-/raw/main/installer | bash
    - ls -lah .secure_files
```
